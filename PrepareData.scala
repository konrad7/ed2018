import java.nio.file.{ Files, Paths }
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.atomic.AtomicLong

import Data._
import akka.Done
import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl._
import akka.util.ByteString

import scala.collection.immutable
import scala.concurrent._
import scala.concurrent.duration.Duration
import scala.util.control.NonFatal

case class Config(
  sourceFile: String,
  outputFile0: String,
  outputFile1: String,
  windowSize: Int,
  lookAhead: Int,
  slidingStep: Int,
  forwardPredictMinutes: Int,
  useAmplitude: Boolean
)

object Config {
  val default = Config(
    sourceFile = "",
    outputFile0 = "",
    outputFile1 = "",
    windowSize = 50,
    lookAhead = 150,
    slidingStep = 5,
    forwardPredictMinutes = 30,
    useAmplitude = false
  )
}

object ConfigParser extends scopt.OptionParser[Config]("anomalies") {
  opt[String]('i', "input")
    .required()
    .valueName("<input file>")
    .action { (option, conf) => conf.copy(sourceFile = option) }

  opt[String]('o', "output0")
    .required()
    .valueName("<output file for 0>")
    .action { (option, conf) => conf.copy(outputFile0 = option) }

  opt[String]('p', "output1")
    .required()
    .valueName("<output file for 1>")
    .action { (option, conf) => conf.copy(outputFile1 = option) }

  opt[Int]('w', "window-size")
    .action { (option, conf) => conf.copy(windowSize = option) }

  opt[Int]('l', "look-ahead")
    .action { (option, conf) => conf.copy(lookAhead = option) }

  opt[Int]('s', "sliding-step")
    .action { (option, conf) => conf.copy(slidingStep = option) }

  opt[Int]('f', "forward-predict-mins")
    .action { (option, conf) => conf.copy(forwardPredictMinutes = option) }

  opt[String]('t', "merge-strategy")
    .valueName("<combine|amplitude>")
    .action((option, conf) => conf.copy(useAmplitude = option == "amplitude"))

  help("help").text("prints this usage text")
}


object Data {
  case class MachineOutputs(values: Seq[Float]) extends AnyVal

  case class Measurement(time: LocalDateTime, outputs: MachineOutputs, failure: Boolean)

  case class GroupedMeasurements(outputsInWindow: Seq[MachineOutputs], futureFailure: Boolean)

  implicit class TimeCompare(val self: LocalDateTime) extends AnyVal {

    def <=(that: LocalDateTime): Boolean = {
      self.isBefore(that) || that == self
    }
    def <(that: LocalDateTime): Boolean = {
      self.isBefore(that)
    }

  }
}

object Format {
  def parse(string: String): Measurement = {
    string.split(",").toSeq match {
      case rawDate +: rawMeasurements :+ rawFailure =>
        val date = LocalDateTime.parse(rawDate.trim.replace("  ", "T"), DateTimeFormatter.ISO_DATE_TIME)
        val failure = rawFailure match {
          case "1" => true
          case "0" => false
        }
        val outputs = rawMeasurements.map(_.toFloat)
        Measurement(date, MachineOutputs(outputs), failure)
    }
  }

  def serialize(groupedMeasurements: GroupedMeasurements): String = {
    val outputs = groupedMeasurements.outputsInWindow.flatMap(_.values).mkString(",")
    val failure = if (groupedMeasurements.futureFailure) "1" else "0"
    s"$outputs $failure\n"
  }
}

object Normalize {
  import scala.collection.JavaConverters._
  private val (mins, maxes) = {
    try {
      Files.readAllLines(Paths.get("ranges.txt")).asScala
        .map(_.trim).filter(_.nonEmpty)
        .map(_.split(" "))
        .map { case Array(min, max) => (min.toFloat, max.toFloat) }
        .unzip
    } catch {
      case NonFatal(e) =>
        println("Error during loading ranges for normalization: " + e.getMessage)
        sys.exit(1)
    }
  }

  println(s"Loaded ${mins.size} ranges")

  private def normalize(value: Float, lowerBound: Float, upperBound: Float): Float = {
    (value - lowerBound) / (upperBound - lowerBound)
  }

  def normalize(outputs: MachineOutputs): MachineOutputs = MachineOutputs {
    for (i <- outputs.values.indices) yield {
      val res = normalize(outputs.values(i), mins(i), maxes(i))
      if (res > 1.0 || res < 0.0) println(outputs.values(i), mins(i), maxes(i), res)
      if (res.isNaN) 0.0f else res
    }
  }

  def normalize(measurement: Measurement): Measurement = {
    measurement.copy(outputs = normalize(measurement.outputs))
  }

}

object Grouping {

  def group(doubleWindow: immutable.Seq[Measurement])(implicit config: Config, failures: AtomicLong): GroupedMeasurements = {
    val (currentWindow, futureWindow) = doubleWindow.splitAt(config.windowSize)
    val endOfMeasurementSeries = currentWindow.last.time
    val maxExpectedTimeOfFailure = endOfMeasurementSeries.plusMinutes(config.forwardPredictMinutes)
    val failureCandidates = futureWindow.takeWhile(_.time <= maxExpectedTimeOfFailure)

    if (futureWindow.forall(_.time < maxExpectedTimeOfFailure)) {
      println("window is too small :/")
    }
    if (futureWindow.isEmpty) {
      println("something is wrong :/")
    }

    val failureHappenedWithinWindow = failureCandidates.exists(_.failure)

    if (failureHappenedWithinWindow) {
      println("YAY, found failure")
      failures.incrementAndGet()
    }

    val outputs = {
      if (config.useAmplitude) {
        val amplitudes = currentWindow.map(_.outputs.values).transpose.map { allMeasurementsFromOneSensor =>
          allMeasurementsFromOneSensor.max - allMeasurementsFromOneSensor.min
        }
        Seq(MachineOutputs(amplitudes))
      } else {
        currentWindow.map(_.outputs)
      }
    }
    GroupedMeasurements(outputs, failureHappenedWithinWindow)
  }

}

object Processing {

  private implicit val system: ActorSystem = ActorSystem("System")
  private implicit val materializer: ActorMaterializer = ActorMaterializer()

  private def toFile(path: String) = Flow[GroupedMeasurements]
    .map(Format.serialize)
    .map(ByteString.fromString)
    .to(FileIO.toPath(Paths.get(path)))

  private def grouped(implicit config: Config, failures: AtomicLong) = {
    FileIO.fromPath(Paths.get(config.sourceFile))
      .via(Framing.delimiter(ByteString("\r\n"), 1024, allowTruncation = false))
      .map(_.utf8String)
      .map(Format.parse)
      .map(Normalize.normalize)
      .sliding(config.windowSize + config.lookAhead, config.slidingStep)
      .map(Grouping.group)
  }

  private def graph(implicit config: Config, failures: AtomicLong) = RunnableGraph.fromGraph(GraphDSL.create(Sink.ignore) { implicit b => ignore =>
    import GraphDSL.Implicits._

    val partition = b.add(Partition[GroupedMeasurements](3, g => if (g.futureFailure) 1 else 0))

    grouped ~> partition.in

    partition.out(0) ~> toFile(config.outputFile0)
    partition.out(1) ~> toFile(config.outputFile1)
    partition.out(2) ~> ignore.in

    ClosedShape
  })

  def runGraph(implicit config: Config, failures: AtomicLong): Future[Done] = graph.run()

  def terminate() = system.terminate()

}

object Main extends App {
  implicit val config: Config = ConfigParser.parse(args, Config.default).getOrElse(sys.exit(1))
  implicit val failures: AtomicLong = new AtomicLong(0)
  val start = System.currentTimeMillis()

  Await.result(Processing.runGraph, Duration.Inf)

  println("DONE")
  println(s"FOUND FAILURES: ${failures.get()}")
  val duration: Double = (System.currentTimeMillis() - start) / 1000.0
  println(s"TOOK $duration seconds")
  Await.result(Processing.terminate(), Duration.Inf)

}