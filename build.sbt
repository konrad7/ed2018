name := "anomalies"

version := "1.0"

scalaVersion := "2.11.8"

resolvers ++= Seq(Resolver.sonatypeRepo("public"))


libraryDependencies ++= Seq(
  "com.github.scopt" %% "scopt" % "3.4.0",
  "com.typesafe.akka" %% "akka-stream" % "2.5.6"
)

assemblyJarName in assembly := "anomalies.jar"