import re
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.multiclass import OneVsRestClassifier

# train data: total 1587498 - [1585956/1542] = 0.000972
# test data: total 396543 - [396149/394] = 0.000995

TRAIN_FILE = 'my_train_data.csv'
TEST_FILE = 'my_test_data.csv'

def read_file(filename):
    A = []
    B = []
    with open(filename) as fp:
        for line in fp:
            tmp = re.split(' ', line.strip())
            data = tmp[0]
            error = tmp[1]

            records = re.split(',', data)
            values = [float(x) for x in records]

            A.append(values)
            B.append(int(error))

    return A, B


X, y = read_file(TRAIN_FILE)

# clf = svm.SVC()
# clf = OneVsRestClassifier(svm.SVC(), n_jobs=-1)
clf = RandomForestClassifier()

clf.fit(X, y)

X2, y2 = read_file(TEST_FILE)
result = clf.predict(X2)

# print y2
# print result

counter00 = 0
counter01 = 0
counter10 = 0
counter11 = 0
total_num = len(y2)
for i in range(total_num):
    if y2[i] == 0 and result[i] == 0:
        counter00 += 1
    elif y2[i] == 0 and result[i] == 1:
        counter01 += 1
    elif y2[i] == 1 and result[i] == 0:
        counter10 += 1
    elif y2[i] == 1 and result[i] == 1:
        counter11 += 1

print "RESULTS:"
print "Total counted:", counter00 + counter01 + counter10 + counter11
print "Total numer:", total_num
print "Matched %:", float(counter00 + counter11)/total_num
print "Not matched %:", float(counter01 + counter10)/total_num

def calculateResult(a, b):
    return 0.0 if b == 0 else float(a)/b

print "Expected 0, Result 0:", counter00, "-", calculateResult(counter00, counter00 + counter01)
print "Expected 0, Result 1:", counter01, "-", calculateResult(counter01, counter00 + counter01)
print "Expected 1, Result 0:", counter10, "-", calculateResult(counter10, counter10 + counter11)
print "Expected 1, Result 1:", counter11, "-", calculateResult(counter11, counter10 + counter11)



